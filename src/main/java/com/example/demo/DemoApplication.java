package com.example.demo;

import org.springframework.boot.SpringApplication;

//Main Class (branch-1)
import org.springframework.boot.autoconfigure.SpringBootApplication;

//Comment (branch-2)
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
